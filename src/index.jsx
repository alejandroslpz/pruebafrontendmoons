import React from "react";
import ReactDOM from "react-dom";
import AppController from "./Controller/AppController";
import AppView from "./View/AppView";
import "./Styles/styles.scss";

ReactDOM.render(
  <React.StrictMode>
    <AppController>
      <AppView />
    </AppController>
  </React.StrictMode>,
  document.getElementById("root")
);
