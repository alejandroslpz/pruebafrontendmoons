import { useContext } from "react";
import appContext from "../Context/appContext";
import Doughnut from "../Components/Doughnut";
import Form from "../Components/Form";
import Loader from "../Components/Loader";

const AppView = () => {
  // Importar context (Controlador)
  const AppContext = useContext(appContext);

  const { charts } = AppContext;

  return (
    <section id="main">
      <div className="container">
        <div className="row doughnut__row">
          {charts.map((chart, index) => (
            <div key={index} className="col-12 col-lg-4 doughnut__column">
              {chart.status ? (
                <>
                  <Doughnut chart={chart} />
                  <Form chart={chart} />
                </>
              ) : (
                <Loader colors={chart.colors} />
              )}
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default AppView;
