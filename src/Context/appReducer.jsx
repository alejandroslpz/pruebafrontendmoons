import {
  MODIFY_TABLET_REVENUE,
  MODIFY_SMARTPHONE_REVENUE,
  UPDATE_CHART_REVENUE,
  MODIFY_TABLET_IMPRESIONS,
  MODIFY_SMARTPHONE_IMPRESIONS,
  UPDATE_CHART_IMPRESIONS,
  MODIFY_TABLET_VISITS,
  MODIFY_SMARTPHONE_VISITS,
  UPDATE_CHART_VISITS,
} from "../types";

const appReducer = (state, action) => {
  switch (action.type) {
    case MODIFY_TABLET_REVENUE: {
      const newArrayTabletR = [...state.charts];

      newArrayTabletR[0].parameters[1].value = action.payload;

      // console.log("modify_tablet_Revenue");

      return {
        ...state,
        charts: newArrayTabletR,
      };
    }

    case MODIFY_SMARTPHONE_REVENUE: {
      const newArraySmartR = [...state.charts];

      newArraySmartR[0].parameters[2].value = action.payload;

      // console.log("modify_smartphone_Revenue");

      return {
        ...state,
        charts: newArraySmartR,
      };
    }

    case UPDATE_CHART_REVENUE: {
      const newArrayUR = [...state.charts];

      newArrayUR[0].status = action.payload;

      // console.log("update_chart_Revenue");

      return {
        ...state,
        charts: newArrayUR,
      };
    }
    case MODIFY_TABLET_IMPRESIONS: {
      const newArrayTabletI = [...state.charts];

      newArrayTabletI[1].parameters[1].value = action.payload;

      // console.log("modify_tablet_impresions");

      return {
        ...state,
        charts: newArrayTabletI,
      };
    }

    case MODIFY_SMARTPHONE_IMPRESIONS: {
      const newArraySmartI = [...state.charts];

      newArraySmartI[1].parameters[2].value = action.payload;

      // console.log(newArraySmartI);

      // console.log("modify_smartphone_impresions");

      return {
        ...state,
        charts: newArraySmartI,
      };
    }

    case UPDATE_CHART_IMPRESIONS: {
      const newArrayUI = [...state.charts];

      newArrayUI[1].status = action.payload;
      // console.log("update_chart_impresions");

      return {
        ...state,
        charts: newArrayUI,
      };
    }
    case MODIFY_TABLET_VISITS: {
      const newArrayTabletV = [...state.charts];

      newArrayTabletV[2].parameters[1].value = action.payload;

      // console.log("modify_tablet_visits");

      return {
        ...state,
        charts: newArrayTabletV,
      };
    }

    case MODIFY_SMARTPHONE_VISITS: {
      const newArraySmartV = [...state.charts];

      newArraySmartV[2].parameters[2].value = action.payload;

      // console.log("modify_smartphone_visits");

      return {
        ...state,
        charts: newArraySmartV,
      };
    }

    case UPDATE_CHART_VISITS: {
      const newArrayUV = [...state.charts];

      newArrayUV[2].status = action.payload;
      // console.log("update_chart_visits");

      return {
        ...state,
        charts: newArrayUV,
      };
    }

    default:
      return state;
  }
};

export default appReducer;
