import { useReducer } from "react";
import {
  MODIFY_TABLET_REVENUE,
  MODIFY_SMARTPHONE_REVENUE,
  UPDATE_CHART_REVENUE,
  MODIFY_TABLET_IMPRESIONS,
  MODIFY_SMARTPHONE_IMPRESIONS,
  UPDATE_CHART_IMPRESIONS,
  MODIFY_TABLET_VISITS,
  MODIFY_SMARTPHONE_VISITS,
  UPDATE_CHART_VISITS,
} from "../types";
import appContext from "../Context/appContext";
import appModel from "../Model/AppModel";
import appReducer from "../Context/appReducer";

const AppState = ({ children }) => {
  // Inicializar el controller
  const [state, dispatch] = useReducer(appReducer, appModel);

  const changeTabletRevenue = (valor) => {
    dispatch({
      type: MODIFY_TABLET_REVENUE,
      payload: valor,
    });
  };

  const changeSmartPhoneRevenue = (valor) => {
    dispatch({
      type: MODIFY_SMARTPHONE_REVENUE,
      payload: valor,
    });
  };

  const updateChartRevenue = (valor) => {
    dispatch({
      type: UPDATE_CHART_REVENUE,
      payload: valor,
    });
  };

  const changeTabletImpresions = (valor) => {
    dispatch({
      type: MODIFY_TABLET_IMPRESIONS,
      payload: valor,
    });
  };

  const changeSmartPhoneImpresions = (valor) => {
    dispatch({
      type: MODIFY_SMARTPHONE_IMPRESIONS,
      payload: valor,
    });
  };

  const updateChartImpresions = (valor) => {
    dispatch({
      type: UPDATE_CHART_IMPRESIONS,
      payload: valor,
    });
  };

  const changeTabletVisits = (valor) => {
    dispatch({
      type: MODIFY_TABLET_VISITS,
      payload: valor,
    });
  };

  const changeSmartPhoneVisits = (valor) => {
    dispatch({
      type: MODIFY_SMARTPHONE_VISITS,
      payload: valor,
    });
  };

  const updateChartVisits = (valor) => {
    dispatch({
      type: UPDATE_CHART_VISITS,
      payload: valor,
    });
  };

  return (
    <appContext.Provider
      value={{
        charts: state.charts,
        changeTabletRevenue,
        changeSmartPhoneRevenue,
        updateChartRevenue,
        changeTabletImpresions,
        changeSmartPhoneImpresions,
        updateChartImpresions,
        changeTabletVisits,
        changeSmartPhoneVisits,
        updateChartVisits,
      }}
    >
      {children}
    </appContext.Provider>
  );
};

export default AppState;
