const Loader = ({ colors }) => {
  return (
    <div className="loader__container">
      <div className="loader__lds_ripple">
        <div style={{ borderColor: colors[1] }}></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loader;
