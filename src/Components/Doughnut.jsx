import {
  Area,
  AreaChart,
  Cell,
  Pie,
  PieChart,
  ResponsiveContainer,
  Sector,
  Tooltip,
} from "recharts";
import { useState, useEffect } from "react";

const Doughnut = ({ chart }) => {
  // Almacenar index
  const [active, setActive] = useState(0);
  // Almacenar suma total para graficas
  const [total, setTotal] = useState(0);

  // Modificar el total al momento de cambios y al iniciar
  useEffect(() => {
    setTotal(chart.parameters[1].value + chart.parameters[2].value);
  }, [total, chart.parameters]);

  // Almacenar la data original de la tabla para uso de la grafica interna
  const originalData = {
    tablet: chart.parameters[1].value,
    smpartphone: chart.parameters[2].value,
  };

  // Cambiar index al hacer hover sobre pie chart
  const onPieEnter = (_, index) => {
    setActive(index);
  };

  // Data de la grafica interna
  const data = [
    {
      name: "Smartphone - Original",
      valor: originalData.smpartphone,
    },
    {
      name: "Tablet - Original",
      valor: originalData.tablet,
    },
    {
      name: "Smartphone - Nuevo",
      valor: chart.parameters[2].value,
    },
    {
      name: "Tablet - Nuevo",
      valor: chart.parameters[1].value,
    },
  ];

  // Funcion para modificacion de parametros en pie chart
  const renderActiveShape = (props) => {
    const {
      cx,
      cy,
      innerRadius,
      outerRadius,
      startAngle,
      endAngle,
      fill,
      payload,
    } = props;

    return (
      <g>
        <text x={cx} y={cy} dy={-20} textAnchor="middle" fill={fill}>
          {payload.name}
        </text>
        {chart.parameters[0].name === "REVENUE" ? (
          <>
            {payload.value === 0 ? (
              <>
                <text
                  style={{ fontWeight: "bold", fontSize: 18 }}
                  x={cx}
                  y={cy}
                  dy={1}
                  textAnchor="middle"
                  fill={fill}
                >
                  {new Intl.NumberFormat("es-MX").format(total)}€
                </text>
              </>
            ) : (
              <>
                <text
                  style={{ fontWeight: "bold", fontSize: 18 }}
                  x={cx}
                  y={cy}
                  dy={1}
                  textAnchor="middle"
                  fill={fill}
                >
                  {new Intl.NumberFormat("es-MX").format(payload.value)}€
                </text>
              </>
            )}
          </>
        ) : (
          <>
            {payload.value === 0 ? (
              <>
                <text
                  style={{ fontWeight: "bold", fontSize: 18 }}
                  x={cx}
                  y={cy}
                  dy={1}
                  textAnchor="middle"
                  fill={fill}
                >
                  {new Intl.NumberFormat("es-MX").format(total)}
                </text>
              </>
            ) : (
              <>
                <text
                  style={{ fontWeight: "bold", fontSize: 18 }}
                  x={cx}
                  y={cy}
                  dy={1}
                  textAnchor="middle"
                  fill={fill}
                >
                  {new Intl.NumberFormat("es-MX").format(payload.value)}
                </text>
              </>
            )}
          </>
        )}
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
      </g>
    );
  };

  return (
    <>
      <div className="doughnut__container">
        <ResponsiveContainer>
          <PieChart>
            <Pie
              activeIndex={active}
              activeShape={renderActiveShape}
              data={chart.parameters}
              innerRadius={70}
              outerRadius={80}
              fill="#8884d8"
              dataKey="value"
              onMouseEnter={onPieEnter}
              onMouseLeave={() => setActive(0)}
            >
              {chart.parameters.map((entry, index) => (
                <Cell
                  key={`cell-${index}`}
                  fill={chart.colors[index % chart.colors.length]}
                />
              ))}
            </Pie>
          </PieChart>
        </ResponsiveContainer>
        <div className="doughnut__chart">
          <ResponsiveContainer>
            <AreaChart data={data}>
              <Tooltip />
              <Area
                type="monotone"
                dataKey="valor"
                stroke={chart.colors[1]}
                fill={chart.colors[1]}
              />
            </AreaChart>
          </ResponsiveContainer>
        </div>
      </div>
      <div className="doughnut__labels_container">
        <div className="doughnut__labels">
          <p style={{ color: chart.colors[1], fontWeight: "bold" }}>Tablet</p>
          <div>
            <p>{Math.round((chart.parameters[1].value / total) * 100)}%</p>
            <p>
              {new Intl.NumberFormat("es-MX").format(chart.parameters[1].value)}
              {chart.parameters[0].name === "REVENUE" ? "€" : null}
            </p>
          </div>
        </div>
        <div className="doughnut__labels">
          <p
            className="doughnut__labels_right"
            style={{ color: chart.colors[2], fontWeight: "bold" }}
          >
            Smartphone
          </p>
          <div>
            <p>{Math.round((chart.parameters[2].value / total) * 100)}%</p>
            <p>
              {new Intl.NumberFormat("es-MX").format(chart.parameters[2].value)}
              {chart.parameters[0].name === "REVENUE" && "€"}
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Doughnut;
