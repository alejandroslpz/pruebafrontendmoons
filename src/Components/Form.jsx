import { useContext, useState } from "react";
import appContext from "../Context/appContext";

const Form = ({ chart }) => {
  // Importar context (Controlador)
  const AppContext = useContext(appContext);

  // Data para modificar de la tabla
  const [data, setData] = useState({
    tablet: chart.parameters[1].value,
    smartphone: chart.parameters[2].value,
  });

  // Importar funciones del reducer para realizar cambios dependiendo de la grafica
  const {
    changeTabletRevenue,
    changeSmartPhoneRevenue,
    updateChartRevenue,
    changeTabletImpresions,
    changeSmartPhoneImpresions,
    updateChartImpresions,
    changeTabletVisits,
    changeSmartPhoneVisits,
    updateChartVisits,
  } = AppContext;

  // Modificar data al modificar informacion del input
  const handleInputChange = (event) => {
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });
  };

  // Funcion para llamar a funciones para modificar datos
  const setChart = () => {
    // Si es grafica 1
    if (chart.parameters[0].name === "REVENUE") {
      changeTabletRevenue(parseInt(data.tablet));
      changeSmartPhoneRevenue(parseInt(data.smartphone));
      updateChartRevenue(false);

      setTimeout(() => {
        updateChartRevenue(true);
      }, 1500);
    }

    // Si es grafica 2
    if (chart.parameters[0].name === "IMPRESIONS") {
      changeTabletImpresions(parseInt(data.tablet));
      changeSmartPhoneImpresions(parseInt(data.smartphone));
      updateChartImpresions(false);

      setTimeout(() => {
        updateChartImpresions(true);
      }, 1500);
    }

    // Si es grafica 3
    if (chart.parameters[0].name === "VISITS") {
      changeTabletVisits(parseInt(data.tablet));
      changeSmartPhoneVisits(parseInt(data.smartphone));
      updateChartVisits(false);

      setTimeout(() => {
        updateChartVisits(true);
      }, 1500);
    }
  };

  return (
    <>
      <div className="form__container">
        <label style={{ color: chart.colors[1] }} htmlFor="tablet">
          Tablet
        </label>
        <input
          style={{ borderColor: chart.colors[1], color: chart.colors[2] }}
          value={data.tablet}
          type="number"
          name="tablet"
          className="form-control"
          id="tablet"
          onChange={handleInputChange}
        />
      </div>
      <div className="form__container">
        <label style={{ color: chart.colors[2] }} htmlFor="smartphone">
          Smartphone
        </label>
        <input
          style={{ borderColor: chart.colors[1], color: chart.colors[2] }}
          value={data.smartphone}
          type="number"
          name="smartphone"
          className="form-control"
          id="smartphone"
          onChange={handleInputChange}
        />
      </div>
      <button
        style={{ borderColor: chart.colors[1], color: chart.colors[2] }}
        className="form__button"
        onClick={setChart}
      >
        Actualizar datos
      </button>
    </>
  );
};

export default Form;
