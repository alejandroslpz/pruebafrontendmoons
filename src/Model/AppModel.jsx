const AppModel = {
  charts: [
    {
      parameters: [
        { name: "REVENUE", value: 0 },
        { name: "Tablet", value: 120000 },
        { name: "Smartphone", value: 80000 },
      ],
      colors: ["", "#81D03E", "#3C611D"],
      status: true,
    },
    {
      parameters: [
        { name: "IMPRESIONS", value: 0 },
        { name: "Tablet", value: 20000000 },
        { name: "Smartphone", value: 30000000 },
      ],
      colors: ["", "#7AC5D6", "#275368"],
      status: true,
    },
    {
      parameters: [
        { name: "VISITS", value: 0 },
        { name: "Tablet", value: 480000000 },
        { name: "Smartphone", value: 120000000 },
      ],
      colors: ["", "#EBBD2A", "#B75727"],
      status: true,
    },
  ],
};

export default AppModel;
